<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

require APPPATH . '/libraries/MY_Controller.php';
class Refindi extends MY_Controller {    
    
    private $aksesmenu = [];
    
    function __construct() {
       parent::__construct();
        //kode menu ref indikator -> 6
       if ($this->session->userdata('usrmsk')==NULL) {
           redirect('main');
       } else {
           $this->aksesmenu = $this->__aksesmenu($this->session->userdata('idunit'));
            if ($this->session->userdata('idunit') !=='1' && !in_array('6', $this->aksesmenu)){
                redirect('main');   
            }
        }
    }
    
    function index(){
        $this->load->model('mref');
        $edit = $idunit = $indi = $uraian = $rumpun = '';
        if ($this->input->post()){
            $edit = $this->input->post('edit');
            $idunit = $this->input->post('idunit');
            $uraian = $this->input->post('uraian');
            $rumpun = $this->input->post('rumpun');
            $indi = $this->input->post('indi');
            if ($edit){
                $this->db->update('ref_indikator', array('indikator'=>$indi, 'uraian'=>$uraian, 'nmrumpun'=>$rumpun, 'idunit'=>$idunit), array('indikator'=>$edit));
                if ($this->db->affected_rows()>0){
                    $this->session->set_flashdata('success', 'Update data BERHASIL');
                } else {
                    $this->session->set_flashdata('error', 'Update data GAGAL!');
                }
            } else {
                $this->db->insert('ref_indikator', array('indikator'=>$indi, 'uraian'=>$uraian, 'nmrumpun'=>$rumpun, 'idunit'=>$idunit));
               if ($this->db->affected_rows()>0){
                    $this->session->set_flashdata('success', 'Tambah data BERHASIL');
                } else {
                    $this->session->set_flashdata('error', 'Tambah data GAGAL!');
                }
            }
            redirect(base_url('refindi'));
        } else if ($this->input->get()) {
            $edit = $this->input->get('edit');
            $hapus = $this->input->get('hapus');
            if ($edit){
                $result = $this->db->get_where('ref_indikator', array('indikator'=>$edit))->row();
                if ($result){
                    $indi = $result->indikator;
                    $uraian = $result->uraian;
                    $idunit = $result->idunit;
                    $rumpun = $result->nmrumpun;
                } 
            } else if($hapus){
                $this->db->delete('ref_indikator', array('indikator'=>$hapus));
                if ($this->db->affected_rows() > 0) {
                    $this->session->set_flashdata('success', 'Penghapusan data BERHASIL.');
                } else {
                    $this->session->set_flashdata('success', 'Penghapusan data GAGAL!');
                }
                redirect(base_url('refindi'));
            }            
        } 
        
        $content = array('akses'=> $this->aksesmenu, 'idunit'=>$idunit, 'uraian'=>$uraian, 'edit'=>$edit, 'indi'=>$indi, 'rumpun'=>$rumpun);
        $data['content']= $content;
        $data['page'] = 'refindipage';
        $this->load->view('main', $data);
    }
}