<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

require APPPATH . '/libraries/MY_Controller.php';
class Refrek extends MY_Controller {    
    
    private $aksesmenu = [];
    
    function __construct() {
       parent::__construct();
        //kode menu ref rek -> 11
       if ($this->session->userdata('usrmsk')==NULL) {
           redirect('main');
       } else {
           $this->aksesmenu = $this->__aksesmenu($this->session->userdata('idunit'));
            if ($this->session->userdata('idunit') !=='1' && !in_array('6', $this->aksesmenu)){
                redirect('main');   
            }
        }
    }
    
    function index(){
        $this->load->model('mref');
        $edit = $kdrek = $nmrek = '';
        if ($this->input->post()){
            $edit = $this->input->post('edit');
            $kdrek = $this->input->post('kdrek');
            $nmrek = $this->input->post('nmrek');
            if ($edit){
                $this->db->update('ref_jenis_rekening', array('Uraian'=>$nmrek), array('Kode'=>$kdrek));
                if ($this->db->affected_rows()>0){
                    $this->session->set_flashdata('success', 'Update data BERHASIL');
                } else {
                    $this->session->set_flashdata('error', 'Update data GAGAL!');
                }
            } else {
                $this->db->insert('ref_jenis_rekening', array('Kode'=>$kdrek, 'Uraian'=>$nmrek));
                if ($this->db->affected_rows()>0){
                    $this->session->set_flashdata('success', 'Tambah data BERHASIL');
                } else {
                    $this->session->set_flashdata('error', 'Tambah data GAGAL!');
                }
            }
            redirect(base_url('refrek'));
        } else if ($this->input->get()) {
            $edit = $this->input->get('edit');
            $hapus = $this->input->get('hapus');
            if ($edit){
                $result = $this->db->get_where('ref_jenis_rekening', array('Kode'=>$edit))->row();
                if ($result){
                    $kdrek = $edit;
                    $nmrek = $result->Uraian;
                } 
            } else if($hapus){
                $this->db->delete('ref_jenis_rekening', array('Kode'=>$hapus));
                if ($this->db->affected_rows() > 0) {
                    $this->session->set_flashdata('success', 'Penghapusan data BERHASIL.');
                } else {
                    $this->session->set_flashdata('success', 'Penghapusan data GAGAL!');
                }
                redirect(base_url('refrek'));
            }            
        } 
        
        if (!$edit){
            $this->db->select_max('Kode', 'last');
            $last = $this->db->get('ref_jenis_rekening')->row()->last;
            $kdrek = (int)$last +1;
        }
        $content = array('akses'=> $this->aksesmenu, 'kdrek'=>$kdrek, 'nmrek'=>$nmrek, 'edit'=>$edit);
        $data['content']= $content;
        $data['page'] = 'refrekpage';
        $this->load->view('main', $data);
    }
}