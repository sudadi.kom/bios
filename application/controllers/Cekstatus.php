<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

require APPPATH . '/libraries/MY_Controller.php';
class Cekstatus extends MY_Controller {    
    
    private $aksesmenu = [];
    
    function __construct() {
       parent::__construct();
        //kode menu layanan kes -> 1
       if ($this->session->userdata('usrmsk')==NULL) {
           redirect('main');
       } else {
           $this->aksesmenu = $this->__aksesmenu($this->session->userdata('idunit'));
            if ($this->session->userdata('idunit') !=='1' && !in_array('1', $this->aksesmenu)){
                redirect('main');   
            }
        }
    }
 
    function index(){
        $status = $tahun = $bulan = $jenis = '';
        $this->load->model('mref');
        $this->load->model('mlayanan');
        $this->load->model('mkeuangan');
        if ($this->input->post()){
            $tahun = $this->input->post('tahun');
            $bulan = $this->input->post('bulan');
            $jenis = $this->input->post('jenis');
            $status = $this->input->post('status');
        }
            
        $content = array('akses'=>$this->aksesmenu, 'jenis'=>$jenis, 'tahun'=>$tahun, 'bulan'=>$bulan, 'status'=>$status);
        $data['content']=$content;
        $data['page'] = 'statuspage';
        $this->load->view('main', $data);
    }
}