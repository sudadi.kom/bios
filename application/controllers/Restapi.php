<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

require APPPATH . '/libraries/REST_Controller.php';

class Restapi extends REST_Controller {
    function __construct($config = 'rest') {
        parent::__construct($config);
        $log = array('time' => date('Y-m-d H:i:s'), 'ip' => $_SERVER['REMOTE_ADDR'], 'url' => $_SERVER['REQUEST_URI']);
        $this->db->insert('accesslog', $log);
    }
    
    function validate($tgl){
        $date = date_parse($tgl);
        if ($date["error_count"] == 0 && checkdate($date["month"], $date["day"], $date["year"]))
            return true;
        else
            $this->response(array('log' => [array('Tanggal' => date('Y/m/d H:m:s'), 'summary'=>'Bad Request', 'errordetail'=> 'server error 400')]), 400);
    }
            
    function index_get(){
        $this->load->view('welcome');
    }
            
    function layanan_kesehatan_get(){
        $tgl = $this->get('tanggal_update');
        if ($this->validate($tgl)) {
            $tgl_upd = date("Y/m/d", strtotime($tgl));
            $this->db->where('DATE(tgl_update)', $tgl_upd);
            $result = $this->db->get('layanan_kesehatan')->result();
            if ($result) {
                foreach ($result as $row){
                    $data['layanan_kesehatan'][] = array(
                        'kode_satker' => $row->kode_satker,
                        'tahun' => $row->tahun,
                        'bulan' => $row->bulan,
                        'kelas' => $row->kelas,
                        'jumlah_pasien' => $row->jumlah_pasien,
                        'jumlah_hari' => $row->jumlah_hari,
                        'tgl_update' => date("Y/m/d H:i:s", strtotime($row->tgl_update))
                    );
                }
                $this->response($data , 200);
            } else {
                $this->response(array('log' => [array('Tanggal' => date('Y/m/d H:i:s'), 'summary'=>'Data Not Found')]), 200);
            }            
        } 
    }
    
    function layanan_lainnya_get() {
         $tgl = $this->get('tanggal_update');
        if ($this->validate($tgl)) {
            $tgl_upd = date("Y/m/d", strtotime($tgl));
            $this->db->where('DATE(tgl_update)', $tgl_upd);
            $result = $this->db->get('layanan_lainnya')->result();
            if ($result) {
                foreach ($result as $row){
                    $data['layanan_lainnya'][] = array(
                        'kode_satker' => $row->kode_satker,
                        'tahun' => $row->tahun,
                        'bulan' => $row->bulan,
                        'indikator' => $row->indikator,
                        'jumlah' => $row->jumlah,
                        'tgl_update' => date("Y/m/d H:i:s", strtotime($row->tgl_update))
                    );
                }
                 $this->response($data, 200);
            } else {
                $this->response(array('log' => [array('Tanggal' => date('Y/m/d H:i:s'), 'summary'=>'Data Not Found')]), 200);
            } 
        } 
    }
    
    function penerimaan_blu_get() {
         $tgl = $this->get('TanggalUpdate');
        if ($this->validate($tgl)) {
            $tgl_upd = date("Y-m-d", strtotime($tgl));
            $this->db->where('DATE(TanggalUpdate)', $tgl_upd);
            $result = $this->db->get('penerimaan')->result();
            if ($result) {
                foreach ($result as $row){
                   $data['Penerimaan'][] = array(
                        'Tanggal' => $row->Tanggal,
                        'KodeAkun' => $row->KodeAkun,
                        'Saldo' => $row->Saldo,
                        'TanggalUpdate' => $row->TanggalUpdate
                    ); 
                }
                $this->response($data, 200);   
            } else {
               $this->response(array('log' => [array('Tanggal' => date('Y-m-d H:i:s'), 'summary'=>'Data Not Found')]), 200); 
            }
        } 
    }
    
    function pengeluaran_blu_get() {
        $tgl = $this->get('TanggalUpdate');
        if ($this->validate($tgl)) {
            $tgl_upd = date("Y-m-d", strtotime($tgl));
            $this->db->where('DATE(TanggalUpdate)', $tgl_upd);
            $result = $this->db->get('pengeluaran')->result();
            if ($result) {
                foreach ($result as $row){
                   $data['Pengeluaran'][] = array(
                        'Tanggal' => $row->Tanggal,
                        'KodeAkun' => $row->KodeAkun,
                        'Saldo' => $row->Saldo,
                        'TanggalUpdate' => $row->TanggalUpdate
                    ); 
                }
                $this->response($data, 200);   
            } else {
               $this->response(array('log' => [array('Tanggal' => date('Y-m-d H:i:s'), 'summary'=>'Data Not Found')]), 200); 
            }
        }
    }
    
    function saldo_blu_get() {
        $tgl = $this->get('TanggalUpdate');
        if ($this->validate($tgl)) {
            $tgl_upd = date("Y-m-d", strtotime($tgl));
            $this->db->where('DATE(TanggalUpdate)', $tgl_upd);
            $result = $this->db->get('saldo')->result();
            if ($result) {
                foreach ($result as $row){
                    $data['SaldoBLU'][] = array(
                        'Tanggal' => $row->Tanggal,
                        'KodeJenisRekening' => $row->KodeJenisRekening,
                        'NamaBank' => $row->NamaBank,
                        'Saldo' => $row->Saldo,
                        'TanggalUpdate' => $row->TanggalUpdate
                    );
                }
                $this->response($data, 200);
            }else {
               $this->response(array('log' => [array('Tanggal' => date('Y-m-d H:i:s'), 'summary'=>'Data Not Found')]), 200); 
            }
        } 
    }
} 



?>

