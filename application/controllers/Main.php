<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

require APPPATH . '/libraries/MY_Controller.php';

class Main extends MY_Controller {
    private $aksesmenu = [];
    function __construct() {
        parent::__construct();
        $this->aksesmenu = $this->__aksesmenu($this->session->userdata('idunit'));
    }
    
    function index(){
        $data['page'] = 'infopage';
        $data['content']['akses']= $this->aksesmenu;
        $this->load->view('main', $data);
    }
    
    function login() {
        $this->form_validation->set_rules('userid', 'userid', 'required|trim');
        $this->form_validation->set_rules('password', 'password', 'required|trim');
        if ($this->form_validation->run() == TRUE) {
            $userid=$this->input->post('userid');
            $password=$this->input->post('password');
            $this->load->model('muser');
            $result=$this->muser->login($userid, $password);
            if ($result) {
                $this->session->set_userdata('usrmsk', 'TRUE');
                $this->session->set_userdata('kdsat', '415567');
                $this->session->set_userdata('userid', $result->userid);
                $this->session->set_userdata('idunit', $result->idunit);
                $this->session->set_userdata('realname', $result->nama);
                $this->session->set_flashdata('success', 'Selamat datang '.$result->nama);
                redirect('main');	
            } else {			
                $this->session->set_flashdata('error', 'Your username or password are incorrect');
            }
        }   
        $data['content']['akses']= $this->aksesmenu;
        $data['page'] = 'login';
        $this->load->view('main', $data);
    }
    
    function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
