<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

require APPPATH . '/libraries/MY_Controller.php';
class Editpass extends MY_Controller {    
    
    private $aksesmenu = [];
    
    function __construct() {
       parent::__construct();
        //kode menu ref indikator -> 8
        if ($this->session->userdata('usrmsk')==NULL) {
           redirect('main');
        } else {
           $this->aksesmenu = $this->__aksesmenu($this->session->userdata('idunit'));
        }
    }
    
    function index(){
        $this->load->model('muser');
        if ($this->input->post()){
            $userid = $this->session->userdata('userid');
            $oldpass = $this->input->post('oldpass');
            $newpass1 = $this->input->post('newpass1');
            $newpass2 = $this->input->post('newpass2');
            if ($newpass1 == $newpass2) {
                $result = $this->muser->login($userid, $oldpass);
                if ($result){
                    $this->db->update('user', array('password'=> sha1($newpass1)), array('userid'=>$userid));
                    if ($this->db->affected_rows() > 0) {
                        $this->session->set_flashdata('success', 'Perubahan Password BERHASIL');
                    } else {
                        $this->session->set_flashdata('error', 'Perubahan Password GAGAL!');
                    }
                }
            }
        }
        $content = array('akses'=> $this->aksesmenu);
        $data['content']= $content;
        $data['page'] = 'editpasspage';
        $this->load->view('main', $data);
    }
    
}