<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

require APPPATH . '/libraries/MY_Controller.php';
class Saldo extends MY_Controller {    
    
    private $aksesmenu = [];
    
    function __construct() {
       parent::__construct();
        //kode menu saldo -> 5
       if ($this->session->userdata('usrmsk')==NULL) {
           redirect('main');
       } else {
           $this->aksesmenu = $this->__aksesmenu($this->session->userdata('idunit'));
            if ($this->session->userdata('idunit') !=='1' && !in_array('5', $this->aksesmenu)){
                redirect('main');   
            }
        }
    }
    
    function index(){
        $this->load->model('mref');
        $this->load->model('mkeuangan');
        if ($this->input->post()){
            $jnsrek = $this->input->post('jnsrek');
            $tahun = $this->input->post('tahun');
            $bulan = $this->input->post('bulan');
            $bank = $this->input->post('bank');
            $jml = (float)$this->input->post('jml');
            $periode = $this->input->post('periode');
            $lastday = mktime(0, 0, 0, 3, 0, 2000);
            if($jml !=0 ){
                if ($periode == 1) {
                    $tgl = date('Y-m-d', mktime(0, 0, 0, $bulan+1, 0, $tahun));
                } else {
                     $tgl = date("Y-m-d", mktime(0, 0, 0, $bulan, 15, $tahun));
                }
                $tglupd = date('Y-m-d H:i:s', mktime(0, 0, date("s"), date("m"), date("d", strtotime("tomorrow")), date("Y")));
                $kdsat = $this->session->userdata('kdsat');
                $this->db->insert('saldo', array('KodeJenisRekening'=>$jnsrek, 'Tanggal'=>$tgl, 
                    'Saldo'=>$jml, 'TanggalUpdate'=>$tglupd, 'NamaBank'=>$bank));
                if ($this->db->affected_rows()>0){
                    $this->session->set_flashdata('success', 'Tambah data BERHASIL');
                } else {
                    $this->session->set_flashdata('error', 'Tambah data GAGAL!');
                }
            } else {
                $this->session->flashdata('error', 'Data tidak dapat di simpan, Jumlah tidak boleh 0');
            }
            redirect(base_url('saldo'));
        } else if ($this->input->get('hapus', true)){
            $hapus = $this->input->get('hapus');
            $this->db->where('id', $hapus);
            $this->db->delete('saldo');
            redirect(base_url('saldo'));
        }
        
        $data['content']['akses']= $this->aksesmenu;
        $data['page'] = 'saldopage';
        $this->load->view('main', $data);
    }
}