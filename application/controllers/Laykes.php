<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

require APPPATH . '/libraries/MY_Controller.php';
class Laykes extends MY_Controller {    
    
    private $aksesmenu = [];
    
    function __construct() {
       parent::__construct();
        //kode menu layanan kes -> 1
       if ($this->session->userdata('usrmsk')==NULL) {
           redirect('main');
       } else {
           $this->aksesmenu = $this->__aksesmenu($this->session->userdata('idunit'));
            if ($this->session->userdata('idunit') !=='1' && !in_array('1', $this->aksesmenu)){
                redirect('main');   
            }
        }
    }
    
    function index(){
        $this->load->model('mref');
        $this->load->model('mlayanan');
        if ($this->input->post()){
            $kelas = $this->input->post('kelas');
            $tahun = $this->input->post('tahun');
            $bulan = $this->input->post('bulan');
            $jmlpas = $this->input->post('jmlpas');
            $jmlhari = $this->input->post('jmlhari');
            $tglupd = date('Y-m-d H:i:s', mktime(0, 0, date("s"), date("m"), date("d", strtotime("tomorrow")), date("Y")));
            $kdsat = $this->session->userdata('kdsat');
            if ($jmlhari != 0 && $jmlpas != 0) {
                $this->db->insert('layanan_kesehatan', array('kelas'=>$kelas, 'tahun'=>$tahun, 'bulan'=>$bulan, 
                    'jumlah_pasien'=>$jmlpas, 'jumlah_hari'=>$jmlhari, 'kode_satker'=>$kdsat, 'tgl_update'=>$tglupd));
                if ($this->db->affected_rows()>0){
                    $this->session->set_flashdata('success', 'Data sudah tersimpan');
                } else {
                    $this->session->set_flashdata('error', 'Data tidak dapat di simpan');
                } 
            }else {
                    $this->session->set_flashdata('error', 'Data tidak dapat di simpan jumlah pasien atau hari salah.');
            }
            redirect(base_url('laykes'));
        } else if ($this->input->get('hapus', true)){
            $hapus = $this->input->get('hapus');
            $this->db->where('id', $hapus);
            $this->db->delete('layanan_kesehatan');
            redirect(base_url('laykes'));
        }
        //$edit = '';
        //$content = array('akses'=> $this->aksesmenu, 'bulan'=>'', 'jmlhari'=>'', 'jmlpas'=>'', 'edit'=>'');
        $content = array('akses'=> $this->aksesmenu);
        $data['content']=$content;
        $data['page'] = 'laykespage';
        $this->load->view('main', $data);
    }
    
}