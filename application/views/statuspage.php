<?php defined('BASEPATH') OR exit('No direct script access allowed');


/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
415567;
?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Status Pengisian Data</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?=form_open(base_url('cekstatus'), 'class="form-horizontal form-label-left" data-parsley-validate');?>
                <div class="form-group">
                    <label class="control-label col-sm-2 col-xs-12" for="tahun">Tahun</label>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <?php 
                        $option = array (''=>'-Tahun-', '2017'=>'2017', '2018'=>'2018', '2019'=>'2019', '2020'=>'2020');
                        echo form_dropdown('tahun', $option, $tahun, 'class="form-control col-sm-12 col-xs-12" id="tahun" required');?>
                    </div>
                    <label class="control-label col-sm-2 col-sm-offset-2 col-xs-12" for="bulan">Bulan</label>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <?php 
                        $option = '';
                        $option[''] = '-Bulan-';
                        for ($i = 0; $i <= 11; ++$i) {
                             $time = strtotime(sprintf('+%d months', $i));
                             $option[date('m', $time)] = date('F', $time);
                        }
                        echo form_dropdown('bulan', $option, $bulan, 'class="form-control col-sm-12 col-xs-12" required');?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2 col-xs-12" for="jenis">Data</label>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <?php 
                            $option='';
                            $option['']='--Jenis Data--';
                            $option[1] = 'Layanan Kesehatan';
                            $option[2] = 'Layanan Lainnya';
                            $option[3] = 'Penerimaan BLU';
                            $option[4] = 'Pengeluaran BLU';
                            $option[5] = 'Saldo BLU';
                            echo form_dropdown('jenis', $option, $jenis, 'class="form-control col-sm-12" required');
                        ?>
                    </div>
                    <label class="control-label col-sm-2 col-xs-12" for="status">Status</label>
                    <div class="col-sm-2 col-xs-12">
                        <?php
                        $option='';
                        $option[0] = 'Belum Update';
                        $option[1] = 'Sudah Update';
                        echo form_dropdown('status', $option, $status, 'class="form-control col-sm-12" required');
                        ?>
                    </div>

                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                        <div class="col-xs-5 col-sm-2 col-sm-offset-5">
                            <?=form_button(array('type'=>'submit', 'class'=>'btn btn-success btn-block', 'content'=>'Tampil &nbsp;<i class="fa fa-eye"></i>'));?>
                        </div>
                </div>
                <?=form_close(); ?>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">

                <div class="table-responsive">
                    <table id="dtables" class="table table-striped table-bordered jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Tahun</th>
                                <th class="column-title">Bulan</th>
                                <th class="column-title">Data</th>
                                <th class="column-title">Tgl. Update</th>
                                <th class="column-title">Satker</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                            if ($status == 1 && ($jenis<=2)) {
                                $stat = 'tgl_update is not null';
                            } else if ($status == 0 && ($jenis<=2)){
                                $stat = 'tgl_update is null';
                            } else if ($status == 1){
                                $stat = 'TanggalUpdate is not null';
                            } else {
                                $stat = 'TanggalUpdate is null';
                            }
                            $result ='';
                            switch ($jenis) {
                                case 1 :
                                    $result=$this->mlayanan->getstat_laykes($bulan, $tahun, $stat);
                                    $unit = 'Rekam Medis';
                                    break;
                                case 2 :
                                    $result=$this->mlayanan->getstat_laylain($bulan, $tahun, $stat);
                                    break;
                                case 3 : 
                                    $result=$this->mkeuangan->getstat_terima($bulan, $tahun, $stat);
                                    $unit = 'Keuangan';
                                    break;
                                case 4:
                                    $result=$this->mkeuangan->getstat_keluar($bulan, $tahun, $stat);
                                    $unit = 'Keuangan';
                                    break;
                                case 5:
                                    $result=$this->mkeuangan->getstat_saldo($bulan, $tahun, $stat);
                                    $unit = 'Keuangan';
                                    break;
                            }
                            $i=0;
                            if ($result){
                                foreach ($result as $row){
                                    $i++; ?>
                                <tr>
                                    <td><?=$row['kode'];?></td>
                                    <td><?=$tahun;?></td>
                                    <td><?=$bulan;?></td>
                                    <td><?=$row['nama'];?></td>
                                    <td><?=$row['tgl_update'];?></td>
                                    <?php if ($jenis == 2){?>
                                    <td><?=$row['unit'];?></td>
                                    <?php } else { ?>
                                    <td><?=$unit;?></td>
                                    <?php } ?>
                                </tr>
                                <?php }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>