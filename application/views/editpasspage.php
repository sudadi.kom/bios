<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//$message = "wrong answer";
//echo "<script type='text/javascript'>alert('$message');</script>";
//$this->session->set_flashdata('warning', 'Sik durung di gawe..!!');
?>
<div class="clearfix"></div>
<div class="row">
    <div class="col-sm-6 col-sm-offset-3 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                    <h2>Ganti Password</h2>
                    <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?=form_open('editpass', 'class="form-horizontal form-label-left" data-parsley-validate methode="POST"');?>
                <div class="form-group">
                    <div class="col-sm-12 col-xs-12">
                        <label class="control-label col-sm-5 col-xs-12" for="jml">Password Lama</label>
                        <div class="col-sm-7 col-xs-12">
                            <?php $attribut = array('name'=>'oldpass', 'type'=>'password', 'class'=>'form-control', 'required'=>'');
                            echo form_input($attribut);?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 col-xs-12">
                        <label class="control-label col-sm-5 col-xs-12" for="jml">Password Baru</label>
                        <div class="col-sm-7 col-xs-12">
                            <?php $attribut = array('name'=>'newpass1', 'type'=>'password', 'class'=>'form-control', 'required'=>'');
                            echo form_input($attribut);?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 col-xs-12">
                        <label class="control-label col-sm-5 col-xs-12" for="jml">Ulang Password Baru</label>
                        <div class="col-sm-7 col-xs-12">
                            <?php $attribut = array('name'=>'newpass2', 'type'=>'password', 'class'=>'form-control', 'required'=>'');
                            echo form_input($attribut);?>
                        </div>
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-sm-offset-4">
                        <?php echo form_button(array('type'=>'submit', 'class'=>'btn btn-success', 'content'=>'Simpan'));
                            echo form_button(array('type'=>'reset', 'class'=>'btn btn-warning', 'content'=>'Batal'));?>
                    </div>
                </div>
            </div>
        </div>
    </div>          
</div>
