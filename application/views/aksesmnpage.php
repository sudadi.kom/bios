<?php defined('BASEPATH') OR exit('No direct script access allowed');


/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
415567
?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Mapping Hak Akses Menu Untuk Unit/Satker</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?=form_open('refuser', 'class="form-horizontal form-label-left" data-parsley-validate');?>
                <div class="form-group">
                    <label for="idunit" class="control-label col-sm-2 col-xs-12">Satker</label>
                    <div class="col-sm-3 col-xs-12">
                        <?php
                        $option[''] = 'Pilih Unit'; 
                        $refunit = $this->mref->getrefunit();
                        foreach ($refunit as $key => $value){
                            $option[$value['id']] = $value['unit'];
                        }
                        echo form_dropdown('idunit', $option, $idunit, 'class="form-control col-sm-12" required onchange="getmnbyunit()" id="myidunit"');?>
                    </div>
                </div>
                <?=form_close();?>
                <div class="clearfix"></div>
                <?php ;?>
            </div>
        </div>
    </div>          
</div>