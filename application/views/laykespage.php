<?php defined('BASEPATH') OR exit('No direct script access allowed');


/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
415567;
?>

<div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Data Layanan Kesehatan</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                <?=form_open('laykes', 'class="form-horizontal form-label-left" data-parsley-validate');?>
                    <div class="form-group">
                        <label class="control-label col-sm-2 col-xs-12" for="kelas">Kelas</label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <?php $option[''] = 'Pilih Kelas'; 
                                $refkelas = $this->mref->getrefkelas();
                                foreach ($refkelas as $key => $value){
                                    $option[$value['kode_kelas']] = $value['nama_kelas'];
                                }
                                echo form_dropdown('kelas', $option, 0, 'class="form-control col-sm-12" required');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 col-xs-12" for="tahun">Tahun</label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <?php 
                            $option = '';
                            if ($tahun ='') $tahun = date('Y');
                            $option = array (''=>'Tahun', '2017'=>'2017', '2018'=>'2018', '2019'=>'2019', '2020'=>'2020');
                            echo form_dropdown('tahun', $option, $tahun, 'class="form-control col-sm-12 col-xs-12" id="tahun" required');?>
                        </div>
                    
                        <label class="control-label col-sm-2 col-xs-12" for="bulan">Bulan</label>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <?php 
                            $option = '';
                            $option ['']= 'Bulan';
                            for ($i = 0; $i <= 11; ++$i) {
                                 $time = strtotime(sprintf('+%d months', $i));
                                 $option[date('m', $time)] = date('F', $time);
                            }
                            echo form_dropdown('bulan', $option, '', 'class="form-control col-sm-12 col-xs-12" required');?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2 col-xs-12" for="jmlpas">Jumlah Pasien</label>
                        <div class="col-sm-2 col-xs-12">
                            <?php $attribut = array('name'=>'jmlpas', 'type'=>'number', 'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'reguired');
                            echo form_input($attribut);?>
                        </div>
                        <label class="control-label col-sm-2 col-sm-offset-1 col-xs-12" for="jmlhari">Jumlah Hari</label>
                        <div class="col-sm-2 col-xs-12">
                            <?php $attribut = array('name'=>'jmlhari', 'type'=>'number', 'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'required');
                            echo form_input($attribut);?>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-sm-offset-4">
                            <div class="col-xs-5 col-sm-5 col-md-4 col-lg-3">
                            <?php //echo form_input(array('name'=>'edit', 'type'=>'hidden', 'value'=>$edit));                            
                            //echo form_button(array('type'=>'submit', 'class'=>'btn btn-success', 'content'=>$edit ? 'Update':'Simpan'));
                            echo form_button(array('type'=>'submit', 'class'=>'btn btn-success btn-block', 'content'=>'Simpan &nbsp;<i class="fa fa-save"></i>'));?>
                            </div>
                            <div class="col-xs-5 col-sm-5 col-md-4 col-lg-3">
                                <a href="<?=base_url('main/laykes');?>" class="btn btn-warning btn-block">Batal &nbsp;<i class="fa fa-undo"></i></a>
                            </div>
                        </div>
                    </div>
                <?=form_close(); ?>
                </div>
            </div>
        </div>
    </div> 
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                    
                    <div class="table-responsive">
                        <table id="dtables" class="table table-striped table-bordered jambo_table bulk_action">
                            <thead>
                                <tr class="headings">
                                    <th class="column-title">#</th>
                                    <th class="column-title">Tahun</th>
                                    <th class="column-title">Bulan</th>
                                    <th class="column-title">Kelas</th>
                                    <th class="column-title">Jml.Pasien</th>
                                    <th class="column-title">Jml.Hari</th>
                                    <th class="column-title">Tgl.Update</th>
                                    <th class="column-title">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $result=$this->mlayanan->getallkes();
                                $i=0;
                                if ($result){
                                    foreach ($result as $row){
                                        $i++;
                                        $id = $row['id']; ?>
                                    <tr>
                                        <td><?=$i;?></td>
                                        <td><?=$row['tahun'];?></td>
                                        <td><?=$row['bulan'];?></td>
                                        <td><?=$row['nama_kelas'];?></td>
                                        <td class="text-right"><?=$row['jumlah_pasien'];?></td>
                                        <td class="text-right"><?=$row['jumlah_hari'];?></td>
                                        <td><?=$row['tgl_update'];?></td>
                                        <td><?php echo anchor(base_url('laykes?hapus='.$id), '<i class="fa fa-trash"></i>', 
                                                array('class'=>'btn btn-danger btn-sm', 'title'=>'Hapus', 'onClick'=>"return confirm('Yakin menghapus data tersebut?')"));?></td>
                                    </tr>
                                    <?php }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>