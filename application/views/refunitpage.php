<?php defined('BASEPATH') OR exit('No direct script access allowed');


/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
415567
?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Referensi Unit / Satker</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?=form_open('refunit', 'class="form-horizontal form-label-left" data-parsley-validate');?>
                <div class="form-group">
                    <label for="idunit" class="control-label col-sm-2 col-xs-12">ID.Unit</label>
                    <div class="col-sm-1 col-xs-12">
                        <?=form_input(array('name'=>'idunit', 'value'=>$idunit, 'class'=>'form-control col-sm-12 col-xs-12', 'readonly'=>'')); ?>
                    </div>
                    <label for="nmunit" class="control-label col-sm-2 col-xs-12">Nama</label>
                    <div class="col-sm-5 col-xs-12">
                        <?=form_input(array('name'=>'nmunit', 'value'=>$nmunit, 'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'')); ?>
                    </div>
                </div>
                <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-sm-offset-4">
                            <?php echo form_input(array('name'=>'edit', 'type'=>'hidden', 'value'=>$edit));?>
                            <button type="submit" class="btn btn-success"><?=$edit ? 'Update ':'Simpan ';?><i class="fa fa-save"></i></button>
                            <a href="<?=base_url('refunit');?>" class="btn btn-warning">Batal <i class="fa fa-undo"></i></a>
                        </div>
                    </div>
                <?=form_close(); ?>
            </div>
        </div>
    </div>          
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="table-responsive col-sm-8 col-sm-offset-2 col-xs-12">
                    <table id="dttables" class="table table-striped table-bordered jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">ID Unit</th>
                                <th class="column-title">Nama Unit/Satker</th>
                                <th class="column-title">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $result=$this->mref->getrefunit();
                            if ($result){
                                foreach ($result as $row){
                                    $id = $row['id']; 
                                    if ($id != '1') { ?>
                                <tr>
                                    <td><?=$row['id'];?></td>
                                    <td><?=$row['unit'];?></td>
                                    <td><?php
                                        echo anchor(base_url('refunit?edit='.$id), '<i class="fa fa-edit"></i>', array('class'=>'btn btn-warning btn-sm', 'title'=>'Edit'));
                                        echo anchor(base_url('refunit?hapus='.$id), '<i class="fa fa-trash"></i>', 
                                        array('class'=>'btn btn-danger btn-sm', 'title'=>'Hapus', 'onClick'=>"return confirm('Yakin menghapus data tersebut?')"));?>
                                    </td>
                                </tr>
                                <?php }
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>