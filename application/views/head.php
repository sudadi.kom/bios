<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>RSO | BIOS</title>

	<!-- Jquery UI -->
    <link href="<?php echo base_url('assets/vendors/jquery/dist/jquery-ui.min.css');?>" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?php echo  base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url('assets/vendors/datatables/css/dataTables.bootstrap.min.css');?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url('assets/vendors/datatables/css/buttons.bootstrap.min.css');?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url('assets/vendors/datatables/css/rowGroup.bootstrap.min.css');?>"/>

    <!-- Font Awesome -->
    <link href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url('assets/css/custom.min.css');?>" rel="stylesheet">
    <!-- Toastr -->
    <link href="<?=base_url('assets/vendors/toastr/build/toastr.min.css');?>" rel="stylesheet" type="text/css">
    <!-- Select2 -->
    <link href="<?=base_url('assets/vendors/select2/dist/css/select2.min.css');?>" rel="stylesheet" type="text/css"/>
   
  </head>
