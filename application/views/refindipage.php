<?php defined('BASEPATH') OR exit('No direct script access allowed');


/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
415567
?>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Referensi Indikator</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <?=form_open('refindi', 'class="form-horizontal form-label-left" data-parsley-validate');?>
                <div class="form-group">
                    <label for="indi" class="control-label col-sm-2 col-xs-12">Kode</label>
                    <div class="col-sm-3 col-xs-12">
                        <?=form_input(array('name'=>'indi', 'value'=>$indi, 'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'')); ?>
                    </div>
                    <label for="uraian" class="control-label col-sm-2 col-xs-12">Indikator</label>
                    <div class="col-sm-5 col-xs-12">
                        <?=form_input(array('name'=>'uraian', 'value'=>$uraian, 'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="rumpun" class="control-label col-sm-2 col-xs-12">Rumpun</label>
                    <div class="col-sm-3 col-xs-12">
                        <?=form_input(array('name'=>'rumpun', 'value'=>$rumpun, 'class'=>'form-control col-sm-12 col-xs-12', 'required'=>'')); ?>
                    </div>
                    <label for="idunit" class="control-label col-sm-2 col-xs-12">Unit</label>
                    <div class="col-sm-3 col-xs-12">
                        <?php
                        $option[''] = 'Pilih Unit'; 
                        $refunit = $this->mref->getrefunit();
                        foreach ($refunit as $key => $value){
                            $option[$value['id']] = $value['unit'];
                        }
                        echo form_dropdown('idunit', $option, $idunit, 'class="form-control col-sm-12" required');?>
                    </div>
                </div>
                <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-sm-offset-4">
                            <?php echo form_input(array('name'=>'edit', 'type'=>'hidden', 'value'=>$edit));?>
                            <button type="submit" class="btn btn-success"><?=$edit ? 'Update ':'Simpan ';?><i class="fa fa-save"></i></button>
                            <a href="<?=base_url('refindi');?>" class="btn btn-warning">Batal <i class="fa fa-undo"></i></a>
                        </div>
                    </div>
                <?=form_close(); ?>
            </div>
        </div>
    </div>          
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="table-responsive col-sm-12 col-xs-12">
                    <table id="dtables" class="table table-striped table-bordered jambo_table bulk_action">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">#</th>
                                <th class="column-title">Kode</th>
                                <th class="column-title">Indikator</th>
                                <th class="column-title">Rumpun</th>
                                <th class="column-title">Unit</th>
                                <th class="column-title">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $i = 0;
                            $result=$this->mref->getrefindijoin();
                            if ($result){
                                foreach ($result as $row){
                                    $id = $row['indikator']; 
                                    if ($id != 1) {
                                    $i++;?>
                                <tr>
                                    <td><?=$i;?></td>
                                    <td><?=$row['indikator'];?></td>
                                    <td><?=$row['uraian'];?></td>
                                    <td><?=$row['nmrumpun'];?></td>
                                    <td><?=$row['unit'];?></td>
                                    <td><?php
                                        echo anchor(base_url('refindi?edit='.$id), '<i class="fa fa-edit"></i>', array('class'=>'btn btn-warning btn-sm', 'title'=>'Edit'));
                                        echo anchor(base_url('refindi?hapus='.$id), '<i class="fa fa-trash"></i>', 
                                        array('class'=>'btn btn-danger btn-sm', 'title'=>'Hapus', 'onClick'=>"return confirm('Yakin menghapus data tersebut?')"));?>
                                    </td>
                                </tr>
                                <?php }
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>