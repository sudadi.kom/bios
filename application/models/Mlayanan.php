<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class Mlayanan extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }
    
    function getallkes() {
        $this->db->from('layanan_kesehatan');
        $this->db->join('ref_kelas', 'layanan_kesehatan.kelas=ref_kelas.kode_kelas');  
        $this->db->order_by("id", "desc");
        return $this->db->get()->result_array();
    }
    
    
    function  getlaylain($idunit = null) {
        $this->db->select('layanan_lainnya.*, ref_indikator.uraian, ref_indikator.idunit');
        $this->db->from('layanan_lainnya');
        $this->db->join('ref_indikator', 'ref_indikator.indikator=layanan_lainnya.indikator');
        if ($idunit != NULL && $idunit != 1) {
            $this->db->where('idunit', $idunit);
        }
        $this->db->order_by("id", "desc");
        return $this->db->get()->result_array();
    }
    
    
    function getstat_laykes($bln, $thn, $stat) {
    //SELECT  `ref_kelas`.*, `layanan_kesehatan`.`tgl_update`
    //FROM  `ref_kelas`
    //LEFT JOIN `layanan_kesehatan` ON `ref_kelas`.`kode_kelas` = `layanan_kesehatan`.`kelas` and  month(tgl_update) = 7 and YEAR(tgl_update)=2017
        if ($bln || $thn || $stat){
        $this->db->select('ref_kelas.kode_kelas as kode, ref_kelas.nama_kelas as nama, layanan_kesehatan.tgl_update');
        $this->db->from('ref_kelas');
        $this->db->join('layanan_kesehatan', 'ref_kelas.kode_kelas=layanan_kesehatan.kelas and bulan='.$bln.' and tahun='.$thn, 'left');
        $this->db->where($stat);
        return $this->db->get()->result_array();
        }
    }
    
    function getstat_laylain($bln, $thn, $stat) {
//      SELECT `layanan_lainnya`.`tgl_update`, `ref_indikator`.`uraian`, `ref_indikator`.`indikator`
//      FROM `ref_indikator`
//      LEFT JOIN `unit` ON `unit`.`id` = `ref_indikator`.`idunit` 
//      LEFT JOIN `layanan_lainnya` ON `ref_indikator`.`indikator` = `layanan_lainnya`.`indikator`  and  month(tgl_update) = '06' and YEAR(tgl_update)=2017
//      where tgl_update is null
        if ($bln || $thn || $stat){
            $this->db->select('unit.unit, layanan_lainnya.tgl_update, ref_indikator.uraian as nama, ref_indikator.indikator as kode');
            $this->db->from('ref_indikator');
            $this->db->join('unit', 'ref_indikator.idunit=unit.id', 'left');
            $this->db->join('layanan_lainnya', 'ref_indikator.indikator=layanan_lainnya.indikator and bulan='.$bln.' and tahun='.$thn, 'left');
            $this->db->where($stat);
            return $this->db->get()->result_array();
        }
    }
}