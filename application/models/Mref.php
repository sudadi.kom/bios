<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class Mref extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    
    function getrefkelas(){
        return $this->db->get('ref_kelas')->result_array();
    }
    
    function getrefindi() {
        return $this->db->get('ref_indikator')->result_array();
    }
    
    function getreftrm(){
        return $this->db->get('ref_akun_penerimaan')->result_array();
    }
    
    function getrefkeluar(){
        return $this->db->get('ref_akun_pengeluaran')->result_array();
    }
    
    function getrefjnsrek(){
        return $this->db->get('ref_jenis_rekening')->result_array();
    }
    
    function getrefunit() {
        //$this->db->where('id != 1');
        return $this->db->get('unit')->result_array();
    }
    
    function getrefuserjoin(){
        $this->db->select('user.*, unit.unit');
        $this->db->join('unit', 'user.idunit=unit.id', 'inner');
        return $this->db->get('user')->result_array();
    }
    
    function getrefindijoin() {
        $this->db->select('ref_indikator.*, unit.unit');
        $this->db->join('unit', 'ref_indikator.idunit=unit.id', 'inner');
        return $this->db->get('ref_indikator')->result_array();
    }
    
    
    function getaksesmenu(){
        $thin->db->select('menu.*, aksesmenu.idunit');
        $this->db->join('aksesmenu', 'menu.id=aksesmenu.idmenu', 'left outer');
        return $this->db->get('menu')->result_array();
    }
  
}