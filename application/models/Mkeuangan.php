<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * The MIT License
 *
 * Copyright 2017 DotKom <sudadi.kom@yahoo.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class Mkeuangan extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }
    
    function getpenerimaan(){
        $this->db->select('penerimaan.*, ref_akun_penerimaan.Uraian');
        $this->db->join('ref_akun_penerimaan', 'ref_akun_penerimaan.Kode=penerimaan.KodeAkun', 'INNER');
        $this->db->order_by("id", "desc");
        return $this->db->get('penerimaan')->result_array();
    }
    
    
    function getpengeluaran(){
        $this->db->select('pengeluaran.*, ref_akun_pengeluaran.Uraian');
        $this->db->join('ref_akun_pengeluaran', 'ref_akun_pengeluaran.Kode=pengeluaran.KodeAkun', 'INNER');
        $this->db->order_by("id", "desc"); 
        return $this->db->get('pengeluaran')->result_array();
    }
    
    
    function getsaldo(){
        $this->db->select('saldo.*, ref_jenis_rekening.Uraian');
        $this->db->join('ref_jenis_rekening', 'ref_jenis_rekening.Kode=saldo.KodeJenisRekening', 'INNER');
        $this->db->order_by("id", "desc");
        return $this->db->get('saldo')->result_array();
    }
    
    function getstat_terima($bln, $thn, $stat){
//  SELECT `ref_akun_penerimaan`.`Kode`, `ref_akun_penerimaan`.`Uraian`, `penerimaan`.`TanggalUpdate`
//  FROM `ref_akun_penerimaan`
//  LEFT JOIN `penerimaan` ON `ref_akun_penerimaan`.`Kode` = `penerimaan`.`KodeAkun` and MONTH(TanggalUpdate)= '06' AND YEAR(TanggalUpdate)= '2017'
//  WHERE TanggalUpdate IS NOT NULL
        if ($bln && $thn && $stat){
            $this->db->select('ref_akun_penerimaan.Kode as kode, ref_akun_penerimaan.Uraian as nama, penerimaan.TanggalUpdate as tgl_update');
            $this->db->from('ref_akun_penerimaan');
            $this->db->join('penerimaan', 'ref_akun_penerimaan.Kode=penerimaan`.KodeAkun and month(Tanggal)='.$bln.' and YEAR(Tanggal)='.$thn, 'left');
            $this->db->where($stat);
            return $this->db->get()->result_array();
        }
    }
    
    function getstat_keluar($bln, $thn, $stat) {
//SELECT `ref_akun_pengeluaran`.*, `pengeluaran`.`TanggalUpdate`
//FROM `ref_akun_pengeluaran`
//LEFT JOIN `pengeluaran` ON `ref_akun_pengeluaran`.`Kode` = `pengeluaran`.`KodeAkun`
        if ($bln && $thn && $stat){
            $this->db->select('ref_akun_pengeluaran.Kode as kode, ref_akun_pengeluaran.Uraian as nama, pengeluaran.TanggalUpdate as tgl_update');
            $this->db->from('ref_akun_pengeluaran');
            $this->db->join('pengeluaran', 'ref_akun_pengeluaran.Kode=pengeluaran.KodeAkun and month(Tanggal)='.$bln.' and year(Tanggal)='.$thn, 'left');
            $this->db->where($stat);
            return $this->db->get()->result_array();
        }
    }
    
    function getstat_saldo($bln, $thn, $stat) {
//SELECT `ref_jenis_rekening`.`Kode`, `ref_jenis_rekening`.`Uraian`, `saldo`.`TanggalUpdate`
//FROM `ref_jenis_rekening`
//LEFT JOIN `saldo` ON `ref_jenis_rekening`.`Kode` = `saldo`.`KodeJenisRekening`
        if ($bln && $thn && $stat) {
            $this->db->select('ref_jenis_rekening.Kode as kode, ref_jenis_rekening.Uraian as nama, saldo.TanggalUpdate as tgl_update');
            $this->db->from('ref_jenis_rekening');
            $this->db->join('saldo', 'ref_jenis_rekening.Kode=saldo.KodeJenisRekening and MONTH(Tanggal)='.$bln.' and YEAR(Tanggal)='.$thn, 'left');
            $this->db->where($stat);
            return $this->db->get()->result_array();
        }
    }
}