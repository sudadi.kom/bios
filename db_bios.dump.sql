-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2017 at 08:23 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bios`
--

-- --------------------------------------------------------

--
-- Table structure for table `aksesmenu`
--

CREATE TABLE `aksesmenu` (
  `id` int(11) NOT NULL,
  `idmenu` varchar(2) NOT NULL,
  `idunit` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aksesmenu`
--

INSERT INTO `aksesmenu` (`id`, `idmenu`, `idunit`) VALUES
(1, '1', '2'),
(2, '2', '2');

-- --------------------------------------------------------

--
-- Table structure for table `layanan_kesehatan`
--

CREATE TABLE `layanan_kesehatan` (
  `id` int(11) NOT NULL,
  `kode_satker` varchar(6) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `kelas` varchar(2) DEFAULT NULL,
  `jumlah_pasien` smallint(3) DEFAULT NULL,
  `jumlah_hari` smallint(3) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `layanan_lainnya`
--

CREATE TABLE `layanan_lainnya` (
  `id` int(11) NOT NULL,
  `kode_satker` varchar(6) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `bulan` varchar(2) DEFAULT NULL,
  `indikator` varchar(4) DEFAULT NULL,
  `jumlah` decimal(6,2) DEFAULT NULL,
  `tgl_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penerimaan`
--

CREATE TABLE `penerimaan` (
  `id` int(11) NOT NULL,
  `Tanggal` datetime NOT NULL,
  `KodeAkun` varchar(7) DEFAULT NULL,
  `Saldo` float NOT NULL,
  `TanggalUpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran`
--

CREATE TABLE `pengeluaran` (
  `id` int(11) NOT NULL,
  `Tanggal` datetime DEFAULT NULL,
  `KodeAkun` varchar(7) DEFAULT NULL,
  `Saldo` float DEFAULT NULL,
  `TanggalUpdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ref_akun_penerimaan`
--

CREATE TABLE `ref_akun_penerimaan` (
  `Kode` varchar(7) NOT NULL,
  `Uraian` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_akun_penerimaan`
--

INSERT INTO `ref_akun_penerimaan` (`Kode`, `Uraian`) VALUES
('424121', 'Pendapatan Layanan Pendidikan Reguler'),
('424122', 'Pendapatan Layanan Pendidikan Non Reguler'),
('424123', 'Pendapatan Layanan Penelitian'),
('424124', 'Pendapatan Short Course'),
('424125', 'Pendapatan Pendidikan Pembentukan'),
('424126', 'Pendapatan Pendidikan Peningkatan'),
('424127', 'Pendapatan Pendidikan Penyegaran'),
('424129', 'Pendapatan Usaha Jasa Layanan Pendidikan Lainnya'),
('4242299', 'Pendapatan Hibah'),
('424311', 'Pendapatan Hasil Kerja Sama Perorangan'),
('424312', 'Pendapatan Hasil Kerja Sama Lembaga/Badan Usaha'),
('424313', 'Pendapatan Hasil Kerja Sama Pemerintah Daerah'),
('424319', 'Pendapatan Hasil Kerja Sama Lainnya'),
('424411', 'Pendapatan Sewa Lahan'),
('424412', 'Pendapatan Sewa Bangunan'),
('424413', 'Pendapatan Sewa Peralatan dan Mesin'),
('424419', 'Pendapatan Sewa lain-lain'),
('424499', 'Pendapatan Usaha BLU Lainnya'),
('424911', 'Pendapatan Bunga Deposito'),
('424912', 'Pendapatan Jasa Giro'),
('424913', 'Pendapatan Jasa Lembaga Keuangan'),
('424914', 'Pendapatan atas Laba Selisih Kurs'),
('424919', 'Pendapatan lain-lain di luar Pendapatan Usaha');

-- --------------------------------------------------------

--
-- Table structure for table `ref_akun_pengeluaran`
--

CREATE TABLE `ref_akun_pengeluaran` (
  `Kode` varchar(7) NOT NULL,
  `Uraian` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_akun_pengeluaran`
--

INSERT INTO `ref_akun_pengeluaran` (`Kode`, `Uraian`) VALUES
('525111', 'Belanja Remunerasi BLU'),
('5251190', 'Belanja Pegawai Selain Remunerasi'),
('5251290', 'Belanja Barang'),
('5251390', 'Belanja Jasa'),
('5251490', 'Belanja Pemeliharaan'),
('5251590', 'Belanja Perjalanan Dinas'),
('5251790', 'Belanja Barang dan Jasa BLU Lainnya'),
('99999', 'Belanja Modal');

-- --------------------------------------------------------

--
-- Table structure for table `ref_indikator`
--

CREATE TABLE `ref_indikator` (
  `indikator` varchar(4) NOT NULL,
  `uraian` varchar(200) DEFAULT NULL,
  `nmrumpun` varchar(50) DEFAULT NULL,
  `idunit` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_indikator`
--

INSERT INTO `ref_indikator` (`indikator`, `uraian`, `nmrumpun`, `idunit`) VALUES
('0034', 'Pasien Rawat Jalan  (Jumlah kunjungan)', 'Kesehatan', '2'),
('0035', 'Pasien Gawat Darurat  (Jumlah kunjungan)', 'Kesehatan', '2'),
('0036', 'Pasien Rawat Inap (Jumlah kunjungan)', 'Kesehatan', '2'),
('0037', 'Pasien Umum (Jumlah kunjungan)', 'Kesehatan', '2'),
('0038', 'Pasien Jaminan (Jumlah kunjungan)', 'Kesehatan', '2'),
('0039', 'Pasien Total (Jumlah kunjungan)', 'Kesehatan', '2'),
('0040', 'Layanan Laboratorium (Jumlah)', 'Kesehatan', '2'),
('0041', 'BOR (%)', 'Kesehatan', '2'),
('0042', 'ALOS (hari)', 'Kesehatan', '2'),
('0043', 'BTO (frekuensi)', 'Kesehatan', '2'),
('0044', 'NDR (%)', 'Kesehatan', '2'),
('0045', 'TOI (hari)', 'Kesehatan', '2'),
('0046', 'Kapasitas Tempat Tidur (Unit)', 'Kesehatan', '2'),
('0129', 'Pasien Rawat Jalan (jumlah orang)', 'Kesehatan', '2'),
('0130', 'Pasien Gawat Darurat (jumlah orang)', 'Kesehatan', '2'),
('0131', 'Pasien Umum (jumlah orang)', 'Kesehatan', '2'),
('0132', 'Pasien Jaminan (jumlah orang)', 'Kesehatan', '2'),
('0133', 'Pasien Total (jumlah orang)', 'Kesehatan', '2'),
('0141', 'Tarif Kelas I (Rp.)', 'Kesehatan', '2'),
('0142', 'Tarif Kelas II (Rp.)', 'Kesehatan', '2'),
('0143', 'Tarif Kelas III (Rp.)', 'Kesehatan', '2'),
('0145', 'Kepatuhan Penggunaan Formularium Nasional / Fornas  (%)', 'Kesehatan', '2'),
('146', 'Ventilator-Associated Pneumonia (VAP) (‰)', 'Kesehatan', '2'),
('147', 'Kepuasan Pelanggan (%)', 'Kesehatan', '2'),
('148', 'POBO (%)', 'Kesehatan', '2'),
('149', 'KRK (%)', 'Kesehatan', '2'),
('150', 'ERT 2 (Menit)', 'Kesehatan', '2'),
('151', 'Ketepatan Identifikasi Pasien (%)', 'Kesehatan', '2'),
('152', 'Clinical Pathway (%)', 'Kesehatan', '2'),
('153', 'PRM (%)', 'Kesehatan', '2'),
('154', 'Penerapan Keselamatan Operasi (%)', 'Kesehatan', '2'),
('155', 'WTOJ (Menit)', 'Kesehatan', '2'),
('156', 'Waktu Lapor Tes Kritis Laboratorium (%)', 'Kesehatan', '2'),
('157', 'WLKK (Hari)', 'Kesehatan', '2'),
('158', 'WLM (Hari)', 'Kesehatan', '2'),
('159', 'Cakupan Kegiatan Pemantapan Mutu Internal (%)', 'Kesehatan', '2'),
('160', 'Emergency Psychiatric Response Time (EPRT) (Menit)', 'Kesehatan', '2'),
('161', 'Waktu Tunggu Penanganan Luka Sepsis Pasien Kusta (Jam)', 'Kesehatan', '2'),
('162', 'Kemampuan Menangani BBLSR 500 gr (%)', 'Kesehatan', '2'),
('163', 'Prosentase Door to Balloon Time 90 menit (Menit)', 'Kesehatan', '2'),
('164', 'Tidak Adanya Kejadian Pasien Jatuh (%)', 'Kesehatan', '2'),
('165', 'ERRT (Menit)', 'Kesehatan', '2'),
('166', 'Pemeriksaan POD (%)', 'Kesehatan', '2'),
('167', 'Pemberian Pencegahan VTE pada Pasien HIP/Knee Arthroplasty (%)', 'Kesehatan', '2'),
('168', 'Waktu Tunggu Rawat Jalan (Menit)', 'Kesehatan', '2'),
('169', 'IAD (‰)', 'Kesehatan', '2'),
('170', 'Angka Kematian di IGD (%)', 'Kesehatan', '2'),
('171', 'WTE (Jam)', 'Kesehatan', '2'),
('178', 'Realisasi Pendapatan PNBP atas layanan dan non layanan (Rp)', 'Kesehatan', '2'),
('219', 'Persentase Kejadian Pasien Jatuh', 'Kesehatan', '2'),
('220', 'Cuci Tangan / Hand Hygiene', 'Kesehatan', '2'),
('221', 'Decubitus<', 'Kesehatan', '2'),
('222', 'Pemberian Anti Platelet 48 Jam pada Pasien Stroke Iskemik', 'Kesehatan', '2'),
('223', 'Pemeriksaan CT-Scan Kepala pada Pasien Stroke 1 jam', 'Kesehatan', '2'),
('224', 'Waktu Tunggu Pelayanan Radiologi (WTPR)', 'Kesehatan', '2');

-- --------------------------------------------------------

--
-- Table structure for table `ref_jenis_rekening`
--

CREATE TABLE `ref_jenis_rekening` (
  `Kode` varchar(7) NOT NULL,
  `Uraian` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_jenis_rekening`
--

INSERT INTO `ref_jenis_rekening` (`Kode`, `Uraian`) VALUES
('1', 'Rekening Operasional'),
('2', 'Rekening Pengelolaan Kas'),
('3', 'Rekening Dana Kelolaan');

-- --------------------------------------------------------

--
-- Table structure for table `ref_kelas`
--

CREATE TABLE `ref_kelas` (
  `kode_kelas` varchar(2) NOT NULL,
  `nama_kelas` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_kelas`
--

INSERT INTO `ref_kelas` (`kode_kelas`, `nama_kelas`) VALUES
('01', 'Di atas kelas I'),
('02', 'Kelas I'),
('03', 'Kelas II'),
('04', 'Kelas III'),
('05', 'Non Kelas');

-- --------------------------------------------------------

--
-- Table structure for table `saldo`
--

CREATE TABLE `saldo` (
  `id` int(11) NOT NULL,
  `Tanggal` datetime DEFAULT NULL,
  `KodeJenisRekening` varchar(1) DEFAULT NULL,
  `NamaBank` varchar(50) DEFAULT NULL,
  `Saldo` float DEFAULT NULL,
  `TanggalUpdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` varchar(2) NOT NULL,
  `unit` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `unit`) VALUES
('1', 'Administrator'),
('2', 'Inst. Rekam Medik'),
('3', 'IGD');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `userid` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `idunit` varchar(2) NOT NULL,
  `level` tinyint(2) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `userid`, `password`, `nama`, `idunit`, `level`, `status`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator', '1', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aksesmenu`
--
ALTER TABLE `aksesmenu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idunit` (`idunit`,`idmenu`) USING BTREE;

--
-- Indexes for table `layanan_kesehatan`
--
ALTER TABLE `layanan_kesehatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kelas` (`kelas`);

--
-- Indexes for table `layanan_lainnya`
--
ALTER TABLE `layanan_lainnya`
  ADD PRIMARY KEY (`id`),
  ADD KEY `indikator` (`indikator`);

--
-- Indexes for table `penerimaan`
--
ALTER TABLE `penerimaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `KodeAkun` (`KodeAkun`);

--
-- Indexes for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `KodeAkun` (`KodeAkun`);

--
-- Indexes for table `ref_akun_penerimaan`
--
ALTER TABLE `ref_akun_penerimaan`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `ref_akun_pengeluaran`
--
ALTER TABLE `ref_akun_pengeluaran`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `ref_indikator`
--
ALTER TABLE `ref_indikator`
  ADD PRIMARY KEY (`indikator`),
  ADD KEY `idunit` (`idunit`);

--
-- Indexes for table `ref_jenis_rekening`
--
ALTER TABLE `ref_jenis_rekening`
  ADD PRIMARY KEY (`Kode`);

--
-- Indexes for table `ref_kelas`
--
ALTER TABLE `ref_kelas`
  ADD PRIMARY KEY (`kode_kelas`),
  ADD KEY `kode_kelas` (`kode_kelas`);

--
-- Indexes for table `saldo`
--
ALTER TABLE `saldo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `KodeJenisRekening` (`KodeJenisRekening`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userid` (`userid`),
  ADD KEY `idunit` (`idunit`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aksesmenu`
--
ALTER TABLE `aksesmenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `layanan_kesehatan`
--
ALTER TABLE `layanan_kesehatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `layanan_lainnya`
--
ALTER TABLE `layanan_lainnya`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `penerimaan`
--
ALTER TABLE `penerimaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `saldo`
--
ALTER TABLE `saldo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `aksesmenu`
--
ALTER TABLE `aksesmenu`
  ADD CONSTRAINT `aksesmenu_ibfk_1` FOREIGN KEY (`idunit`) REFERENCES `unit` (`id`);

--
-- Constraints for table `layanan_kesehatan`
--
ALTER TABLE `layanan_kesehatan`
  ADD CONSTRAINT `layanan_kesehatan_ibfk_1` FOREIGN KEY (`kelas`) REFERENCES `ref_kelas` (`kode_kelas`);

--
-- Constraints for table `layanan_lainnya`
--
ALTER TABLE `layanan_lainnya`
  ADD CONSTRAINT `layanan_lainnya_ibfk_1` FOREIGN KEY (`indikator`) REFERENCES `ref_indikator` (`indikator`);

--
-- Constraints for table `penerimaan`
--
ALTER TABLE `penerimaan`
  ADD CONSTRAINT `penerimaan_ibfk_1` FOREIGN KEY (`KodeAkun`) REFERENCES `ref_akun_penerimaan` (`Kode`);

--
-- Constraints for table `pengeluaran`
--
ALTER TABLE `pengeluaran`
  ADD CONSTRAINT `pengeluaran_ibfk_1` FOREIGN KEY (`KodeAkun`) REFERENCES `ref_akun_pengeluaran` (`Kode`);

--
-- Constraints for table `ref_indikator`
--
ALTER TABLE `ref_indikator`
  ADD CONSTRAINT `ref_indikator_ibfk_1` FOREIGN KEY (`idunit`) REFERENCES `unit` (`id`);

--
-- Constraints for table `saldo`
--
ALTER TABLE `saldo`
  ADD CONSTRAINT `saldo_ibfk_1` FOREIGN KEY (`KodeJenisRekening`) REFERENCES `ref_jenis_rekening` (`Kode`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`idunit`) REFERENCES `unit` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
